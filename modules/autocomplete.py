import readline
readline.parse_and_bind('tab: complete')


class AutoComplete:
    def complete(self, text, state):
        if state == 0:  # on first trigger, build possible matches
            if text:  # cache matches (entries that start with entered text)
                self.matches = [s for s in self.options 
                                    if s and s.startswith(text)]
            else:  # no text entered, all matches possible
                self.matches = self.options[:]
        # return match indexed by state
        try: 
            return self.matches[state]
        except IndexError:
            return None
    def __call__(self,options = [],input_text = 'ac:'):
        readline.set_completer(self.complete)
        readline.set_completer_delims('')
        self.options = options
        tinput = input(input_text)
        return tinput

autocomplete=AutoComplete()

class AutoCompleteWords:
    def complete(self, text, state):
        if state == 0:  # on first trigger, build possible matches
            words = [word  for node in self.options for word in node.split(' ')]
            if text:  # cache matches (entries that start with entered text)
                if ' ' not in text and text not in words:
                    self.matches = [word for word in words if word.startswith(text)]
                elif text in words and any([word.startswith(text) for word in words if text!=word]):
                    tmp = [s for s in self.options if text in s.split(' ')]
                    tmp2 = [word for word in words if word.startswith(text)]
                    self.matches = [text,*tmp,*tmp2]
                elif text in words:
                    tmp = [s for s in self.options if text in s.split(' ')]
                    self.matches = [text,*tmp]
                else:
                    self.matches = [x for x in self.options if x.startswith(text)]
            else:  # no text entered, all matches possible
                #self.matches = self.options[:]
                self.matches = ['',*self.options]
        # return match indexed by state
        try: 
            return self.matches[state]
        except IndexError:
            return None
    def __call__(self,options = [],input_text = 'ac:'):
        readline.set_completer(self.complete)
        readline.set_completer_delims('')
        self.options = options
        tinput = input(input_text)
        return tinput

acwords = AutoCompleteWords()
