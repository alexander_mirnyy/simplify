import os
import modules.clock as clock
db_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),'db')
#file = os.path.join(dir_path,"db.txt")


def read():
    file = os.path.join(db_path,sorted(os.listdir(db_path))[-1])
    with open(file,encoding = 'utf-8') as f:
        nodes = f.read().split('\n')
    return nodes

def write(tmp):
    file = os.path.join(db_path,'db_'+clock.setTime()+'.txt')
    with open(file,mode = 'w',encoding = 'utf-8') as f:
        f.write('\n'.join(tmp))
