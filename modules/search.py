import modules.autocomplete as autocomplete
import modules.inout as inout


#that's the main search function.
def search(tmp = ''):
    nodes = inout.read()
    if tmp == '':
        tmp = autocomplete.autocomplete(input_text = 'search exact string:',options = [])
    for node in nodes:
        if tmp in node:
            print(node)

#everything below should be considered as an addition and being unnecessary
def hashSplit(tmp = ''):
    nodes = inout.read()
    if tmp == '':
        tmp = autocomplete.autocomplete(input_text = 'hashSplit:',options = tags())
    if tmp!='':
        for node in nodes:
            if tmp in node:
                for tmp4 in node.split(' '):
                    if tmp4.endswith(tmp):
                        print(node)

def tags():
    tags = []
    nodes = inout.read()
    for node in nodes:
        if '#' in node:
            node = node.split(' ')
            for tmp2 in node:
                tags.append(tmp2[tmp2.index('#'):]) if '#' in tmp2 else None
    return tags
