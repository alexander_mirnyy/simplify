import modules.inout as inout
import modules.clock as clock
import modules.autocomplete as autocomplete



def edit(before = None,after = None):
    nodes = inout.read()
    if before == None:
        before = autocomplete.autocomplete(input_text = 'before:', options = nodes)
    if after == None:
        after = autocomplete.autocomplete(input_text = 'after:', options = [before])
    nodes = inout.read()

    if all([before == '' or before not in nodes,after !='']):
        #add
        nodes.append('['+clock.setTime()+'] '+after)
        print('added,',end = '')
    elif before in nodes and after!='':
        #edit
        nodes[nodes.index(before)] = after
        print('edited',end = '')
    elif before in nodes and after =='':
        #del
        del nodes[nodes.index(before)]
        print('deleted',end = '')
    else:
        print('omitted,',end = '')
    inout.write(nodes)
